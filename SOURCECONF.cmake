SET( ${TARGETLIB}_WRAP_UI
)

SET( ${TARGETLIB}_SOURCES
	YQWE.cc
	YMGA_QCBTable.cc
	YMGAQWidgetFactory.cc
)

SET( ${TARGETLIB}_HEADERS
  ##### Here go the headers
	YQWE.h
	YMGA_QCBTable.h
	YMGAQWidgetFactory.h
)

SET( EXAMPLES_LIST
  ##### Here go the examples
)

